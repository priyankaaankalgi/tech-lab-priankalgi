Tasks
Please keep some notes as you progress through the tasks below with thoughts, solutions etc... These can contain whatever you like but should generally cover what you changed and why. These notes will be submitted at the end of the exercise.
1) We're going to be using Ansible to deploy some configuration on the Master and Worker nodes. To help with this, we'll create a deployment user. Do the following:
Create a user called "deploy" on both the master and worker nodes

Created a user called deploy : Its in users.yml file and i have used ssh keys to make it secure.

2) Navigate to /vagrant/ansible. This directory contains a number of .yml files for use with ansible.

3) Create and populate an ansible inventory file in /vagrant/ansible so that it has:

A host group called "masters" that contains a single host called "master1"
A host group called "workers" that contains a single host called "worker1"
Configure both hosts to use the "deploy" user you created earlier

I have created a host file.Its in inventory/host.yml 

4) Look at the kube-repos.yml file. Replace the hardcoded Kubernetes apt-key URL so that it can be defined per-host. Please use a suitable variable name.
 
 I have used a variable and its in inventory/group_vars/main.yml 

5) Apply the kube-repos.yml

6) Apply the kube-deps.yml playbook. The play will fail, fix the errors and apply again!

I have removed the aptitude task as it was not necessary gave the version number as vars and called it in group_vars as you can chanhge whenevr necessary instaed of hardcoding.


7) Apply the playbook kube-master.yml. Fix any errors that occur and apply it again. Ensure any changes you make manually are also included in the kube-master.yml playbook so this issue doesn't arise again.

I have setup the ssh key to the user kube.

8) Verify that the master node is functioning correctly on its own. If it isn't, fix any issues. A good way to test this may be to list out all the system pods in a "running" state. Include this in your notes.

yes the pods are in running state

9) Run the kube-workers.yml playbook. Fix any issues that arise during the cluster join. It might be nice to amend the playbooks with any fixes you make.

changed the registered var to join_command_raw

10) Run a few basic commands to check that the cluster is configured, showing pods running and nodes ready. Include the output in your notes.
